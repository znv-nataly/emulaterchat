package ru.dev.chat;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.regex.*;

public class ActivityMain extends AppCompatActivity {

    private LinearLayout llMessages;
    private ScrollViewEndPoint svMessages;
    private EditText etMessage;

    private final int MENU_ID_CLEAR = 1;

    private Random random;
    private Handler handler;

    private AlertDialog.Builder alertDialogBuilder;
    private Runnable runnableGenerateMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            llMessages = (LinearLayout) findViewById(R.id.llMessages);
            svMessages = (ScrollViewEndPoint) findViewById(R.id.svMessages);
            etMessage = (EditText) findViewById(R.id.etMessage);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            finish();
        }

        try {
            assert getSupportActionBar() != null;

            // не показывать заголовок окна
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            // отображение иконки на верхней панели
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_chat);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            finish();
        }

        try {
            // получаем список всех сообщений
            Cursor cursor = DB.getInstance(this).getAllMessage();
            if (cursor.moveToFirst()) {
                do {
                    viewMessage(cursor);
                } while (cursor.moveToNext());
            }
            // прокручиваем список сообщений до конца
            scrollViewToEnd();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            handler = new Handler();
            runnableGenerateMessage = new Runnable() {
                @Override
                public void run() {
                    try {
                        generateMessage();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    } finally {
                        // случайный интервал в миллисекундах в пределах 5 минут
                        int interval = getRandom().nextInt(300000);
                        handler.postDelayed(runnableGenerateMessage, interval);
                    }
                }
            };
            runnableGenerateMessage.run();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_ID_CLEAR, 0, R.string.menu_clear)
                .setIcon(android.R.drawable.ic_menu_delete)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == MENU_ID_CLEAR) {
            if (alertDialogBuilder == null) {
                alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder
                        .setTitle(R.string.title_confirm_dialog)
                        .setMessage(R.string.message_confirm_dialog)
                        .setIcon(android.R.drawable.ic_menu_delete);
                alertDialogBuilder.setPositiveButton(R.string.btn_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            // удаление всех сообщений
                            DB.getInstance(getApplicationContext()).deleteAllMessages();
                            llMessages.removeAllViews();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
                alertDialogBuilder.setNegativeButton(R.string.btn_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
            }
            alertDialogBuilder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Получение объекта random
     * @return random
     */
    public Random getRandom() {
        random = (random == null) ? new Random(System.currentTimeMillis()) : random;
        return random;
    }

    public LinearLayout getLLMessages() {
        return llMessages != null ? llMessages : (LinearLayout) findViewById(R.id.llMessages);
    }

    public ScrollViewEndPoint getSVMessages() {
        return svMessages != null ? svMessages : (ScrollViewEndPoint) findViewById(R.id.svMessages);
    }

    public EditText getETMessage() {
        return etMessage != null ? etMessage : (EditText) findViewById(R.id.etMessage);
    }

    /**
     * Визуализиует сообщение на экране
     * @param cursor данные о сообщении
     */
    public void viewMessage(Cursor cursor) {

        //int idMessage = cursor.getInt(cursor.getColumnIndex(DB.COLUMN_ID));
        int numSender = cursor.getInt(cursor.getColumnIndex(DB.COLUMN_NUM_SENDER));
        String message = cursor.getString(cursor.getColumnIndex(DB.COLUMN_MESSAGE));
        message = matchHashtagInMessage(message);
        String dateAdd = cursor.getString(cursor.getColumnIndex(DB.COLUMN_DATE_ADD));

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item, getLLMessages(), false);

        TextView tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        assert tvMessage != null;
        tvMessage.setText(Html.fromHtml(message));

        TextView tvDateAdd = (TextView) view.findViewById(R.id.tvDateAdd);
        assert tvDateAdd != null;
        tvDateAdd.setText(dateAdd);

        // границы сообщения
        int background = R.drawable.background_border_1;
        // выравнивание сообщения на экране
        int gravity = Gravity.LEFT;

        if (numSender == DB.NUM_SENDER_2) {
            background = R.drawable.background_border_2;
            gravity = Gravity.RIGHT;
        }
        view.findViewById(R.id.llMessageInfo).setBackgroundResource(background);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = gravity;

        getLLMessages().addView(view, layoutParams);
    }

    /**
     * Если в тексте есть hashtag, то выделяет его красным цветом
     * @param message текст сообщения
     * @return текст сообщения с выделенным красным цветом хэштэгами
     */
    public String matchHashtagInMessage(String message) {
        Pattern pattern = Pattern.compile("#\\d*_*[a-zA-Zа-яА-Я]+\\w+");
        Matcher matcher = pattern.matcher(message);

        String hashtag = "";
        String newMessage = message;
        while (matcher.find()) {
            hashtag = message.substring(matcher.start(), matcher.end());
            newMessage = newMessage.replace(hashtag, "<font color='red'>" + hashtag + "</font>");
        }
        return newMessage;
    }

    /**
     * Обработчик нажатия кнопки "Отправить"
     * @param v кнопка @+id/btnSend
     */
    public void onClickSend(View v) {
        try {
            String message = getETMessage().getText().toString().trim();

            if (message.equals("")) {
                Toast.makeText(this, R.string.empty_message, Toast.LENGTH_SHORT).show();
                return;
            }

            long idMessage = DB.getInstance(this).addMessage(DB.NUM_SENDER_2, message);
            Cursor cursor = DB.getInstance(this).getMessageById(idMessage);
            viewMessage(cursor);
            etMessage.setText("");
            if (getSVMessages().scrollEnd) {
                // если прокрутка списка сообщений находилась в конце, то после добавления нового сообщения
                // ее также следуют установить в конец, чтобы было видно последнее сообщение
                scrollViewToEnd();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Прокручивает список сообщений до конца
     */
    public void scrollViewToEnd() {
        getSVMessages().post(new Runnable() {
            @Override
            public void run() {
                getSVMessages().fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    @Override
    protected void onDestroy() {
        try {
            super.onDestroy();
            // закрытие соединения с БД
            DB.getInstance(this).closeConnection();

            // останавливаем генерацию сообщений
            handler.removeCallbacks(runnableGenerateMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Генерация сообщения "от собеседника"
     * @throws Exception
     */
    public void generateMessage() throws Exception {

        // массив шаблонных сообщений
        String[] templateMessage = getResources().getStringArray(R.array.template_message);
        // генерируем случайное число в пределах количества шаблонных сообщений
        int numTemplateMessage = getRandom().nextInt(templateMessage.length);

        long idMessage = DB.getInstance(this).addMessage(DB.NUM_SENDER_1, templateMessage[numTemplateMessage]);
        if (idMessage > 0) {
            Cursor cursor = DB.getInstance(this).getMessageById(idMessage);
            viewMessage(cursor);
            if (getSVMessages().scrollEnd) {
                // если прокрутка списка сообщений находилась в конце, то после добавления нового сообщения
                // ее также следуют установить в конец, чтобы было видно последнее сообщение
                scrollViewToEnd();
            } else {
                Toast.makeText(this, R.string.incoming_message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
