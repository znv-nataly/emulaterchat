package ru.dev.chat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Класс для работы с БД
 */
public class DB {
    private static final String DB_NAME = "db_chat";
    private static final int DB_VERSION = 1;
    private static final String TABLE_CHAT = "chat";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NUM_SENDER= "num_sender";
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_DATE_ADD = "date_add";

    public static final int NUM_SENDER_1 = 1;
    public static final int NUM_SENDER_2 = 2;

    private static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_CHAT + " (" +
                    COLUMN_ID + " integer primary key, " +
                    COLUMN_NUM_SENDER + " integer, " +
                    COLUMN_MESSAGE + " text, " +
                    COLUMN_DATE_ADD + " integer DEFAULT 0 " +
                    ");";

    private static final String[] COLUMNS = new String[] { COLUMN_ID, COLUMN_NUM_SENDER, COLUMN_MESSAGE,
            "strftime('%d-%m-%Y %H:%M', " + COLUMN_DATE_ADD + ", 'unixepoch', 'localtime') as " + COLUMN_DATE_ADD };

    private Context context;
    private DBHelper dbHelper;
    private SQLiteDatabase db;

    private DB (Context context) {
        this.context = context;
    }

    private static DB instance;

    public static DB getInstance(Context context) {
        if (instance == null) {
            instance = new DB(context);
        }

        return instance;
    }

    /**
     * Открытие соединения с БД
     */
    public void openConnection() {
        if (db != null && db.isOpen()) {
            return;
        }
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
        db = dbHelper.getWritableDatabase();
    }

    /**
     * Закрытие соединения с БД
     */
    public void closeConnection() {
        try {
            if (dbHelper != null) dbHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Проверка открытости соединения с БД
     */
    public void checkConnection() {
        if (db == null || !db.isOpen()) {
            openConnection();
        }
    }

    /**
     * Получение диалога, списка всех сообщений, упорядоченных по дате добавления
     * @return список сообщений
     */
    public Cursor getAllMessage() {
        checkConnection();
        return db.query(TABLE_CHAT, COLUMNS, null, null, null, null, COLUMN_DATE_ADD);
    }

    /**
     * Получение сообщения по его ИД
     * @param id ИД сообщения
     * @return cursor
     * @throws Exception
     */
    public Cursor getMessageById(long id) throws Exception {
        checkConnection();
        Cursor cursor = db.query(TABLE_CHAT, COLUMNS, COLUMN_ID + " = " + id, null, null, null, null, "1");
        if (!cursor.moveToFirst()) {
            throw new Exception("Error get data.");
        }
        return cursor;
    }

    /**
     * Добавление в БД отправленного сообщения
     * @param numSender номер отправителя
     * @param message текст сообщения
     * @return ИД вставленной записи
     * @throws Exception
     */
    public long addMessage(int numSender, String message) throws Exception {
        int time = (int)((new java.util.Date()).getTime()/1000);
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NUM_SENDER, numSender);
        cv.put(COLUMN_MESSAGE, message);
        cv.put(COLUMN_DATE_ADD, time);

        checkConnection();
        long id = db.insert(TABLE_CHAT, null, cv);
        if (id <= 0) {
            throw new Exception("Error insert data.");
        }

        return id;
    }

    /**
     * Удаление всех сообщений
     */
    public void deleteAllMessages() {
        db.delete(TABLE_CHAT, null, null);
    }

    // Класс для создания и управления БД
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, dbName, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // создаем таблицу TABLE_CHAT
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
