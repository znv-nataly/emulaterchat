package ru.dev.chat;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

/**
 * ScrollView с возможностью определения, находится ли прокрутка списка в конечной точке
 */
public class ScrollViewEndPoint extends ScrollView {

    // флаг, определяющий, находится ли прокрутка списка в конечной точке
    public boolean scrollEnd = false;

    public ScrollViewEndPoint(Context context) {
        super(context);
    }

    public ScrollViewEndPoint(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollViewEndPoint(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        // у последнего дочернего элемента ScrollView определяем нижнюю позицию
        View view = getChildAt(getChildCount() - 1);

        // вычисляем разницу
        int diff = view.getBottom() - (getHeight() + getScrollY());
        scrollEnd = diff <= 0;

        super.onScrollChanged(l, t, oldl, oldt);
    }
}
